function stopAllOtherPlayers(event) {

    var audioElements = document.getElementsByTagName('audio');
    var thisElement = event.target;

    for(var i = 0; i < audioElements.length; i++) {

        var audio = audioElements[i];
        if(audio != thisElement) { // If this element is not the one that was clicked, stop audio
            audio.pause(); 
        }
    }
}