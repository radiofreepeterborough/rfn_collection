<?php

namespace Drupal\rfn_collection\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure RFN Collection settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'rfn_collection_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['rfn_collection.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['streaming_url_base'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Streaming Url Base'),
      '#default_value' => $this->config('rfn_collection.settings')->get('streaming_url_base'),
      '#description' => $this->t('This is the base uri you will stream audio from.  The final url passed to the audio player will consist of this url + the field_media_streaming_uri value from the track entity')
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!$form_state->getValue('streaming_url_base')) {
      $form_state->setErrorByName('streaming_url_base', $this->t('You must set a value for Streaming Url Base in order to stream audio.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('rfn_collection.settings')
      ->set('streaming_url_base', $form_state->getValue('streaming_url_base'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
