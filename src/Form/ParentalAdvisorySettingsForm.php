<?php

namespace Drupal\rfn_collection\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure RFN Collection settings for this site.
 */
class ParentalAdvisorySettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'rfn_collection_parental_advisory_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['rfn_collection.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['parental_advisory_logo_uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URI to the parental advisory logo'),
      '#default_value' => $this->config('rfn_collection.settings')->get('parental_advisory_logo_uri'),
      '#description' => $this->t('The url to the parental advisory logo to show on collections and tracks marked as Parental Advisory for potentially offensive content'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('rfn_collection.settings')
      ->set('parental_advisory_logo_uri', $form_state->getValue('parental_advisory_logo_uri'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
