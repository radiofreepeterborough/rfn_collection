<?php

namespace Drupal\rfn_collection;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Twig extension.
 */
class RfnCollectionTwigExtension extends \Twig_Extension {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new \Twig_SimpleFunction('rfn_build_streaming_uri', function ($stub = NULL) {
        $config = \Drupal::service('config.factory')->getEditable('rfn_collection.settings');
        $base = $config->get('streaming_url_base');

        return $base . $stub;
      }),
      new \Twig_SimpleFunction('rfn_parental_advisory_logo', function ($size = NULL) {
        $config = \Drupal::service('config.factory')->getEditable('rfn_collection.settings');
        $paLogo = $config->get('parental_advisory_logo_uri');
        $alt = $this->t('Warning - potentially offensive explicit lyrics');
        if($size == 'small' || !$size) {
          return "<img src='$paLogo' alt='$alt' class='parental-advisory-small' style='max-width: 80px;'>";
        }
        
      }),
    ];
  }

}
